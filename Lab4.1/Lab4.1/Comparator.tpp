template<typename Type>
int Comparator<Type>::operator()(mNode<Type>* a, mNode<Type>* b)
{
	if ((*a).getKey() < (*b).getKey()) return -1;
	else if ((*a).getKey() > (*b).getKey()) return 1;
	else return 0;
}

template<typename Type>
int Comparator<Type>::operator()(int a, int b)
{
	if (a < b) return -1;
	else if (a > b) return 1;
	else return 0;
}

