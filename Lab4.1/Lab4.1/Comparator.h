#pragma once
#include "mNode.h"

template <typename Type>
class Comparator
{
public:
	int operator() (mNode<Type>*, mNode<Type>*);
	int operator() (int, int);
};

#include "Comparator.tpp"