#pragma once
#include "Node.h"

template <typename Type>
class List
{
private:
	Node<Type> *first;//wska�nik na pierwszy element
	Node<Type> *last;//wska�nik na ostatni element

public:
	List();
	~List();

	Node<Type>* getFirst() { return first; }
	Node<Type>* getLast() { return last; }

	int size();
	void print();
	void pushFront(Type a);
	void pushBack(Type a);
	void delFront();
	void remove();
	bool isEmpty();
};

#include "List.tpp"