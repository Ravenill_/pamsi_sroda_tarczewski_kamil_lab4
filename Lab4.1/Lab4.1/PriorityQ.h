#pragma once
#include "SeqNode.h"
#include "List.h"

template <typename Type>
class PriorityQ : private SeqNode<Type>
{
public:
	PriorityQ();
	~PriorityQ();

	void insert(int k, Type obj);
	Type removeMin();
	Type min();
	int size();
	bool isItEmpty();
};

template <typename Type>
void prioritySort(List<Type>&);

#include "PriorityQ.tpp"