#pragma once
#include "mNode.h"

template <typename Type>
class dList
{
protected:
	mNode<Type> *first;//wska�nik na pierwszy element
	mNode<Type> *last;//wska�nik na ostatni element
	int size;

public:
	dList();
	~dList();

	int rozmiar();
	bool isEmpty();
	Type pierwszy();
	Type ostatni();
	void wstawPierwszy(Type obj);
	Type usunPierwszy();
	void wstawOstatni(Type obj);
	Type usunOstatni();
	void wstaw(Type obj, mNode<Type>* here, int k);
	Type usun(mNode<Type>* here);
};

#include "dList.tpp"
