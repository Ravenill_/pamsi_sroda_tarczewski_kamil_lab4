#include <iostream>
#include "PriorityQ.h"

int main()
{
	PriorityQ<int> a;
	List<int> b;

	std::cout << "Dodaje 3 z kluczem 1\n";
	a.insert(1, 3);
	std::cout << "Dodaje 2 z kluczem 2\n";
	a.insert(2, 2);
	std::cout << "Dodaje 1 z kluczem 3\n";
	a.insert(3, 1);

	std::cout << "size: " << a.size() << "\n\n";
	std::cout << "top: " << a.min() << "\n";
	std::cout << "IsEmpty: " << a.isItEmpty() << "\n";
	std::cout << "Remove1: " << a.removeMin() << "\n";
	std::cout << "Remove2: " << a.removeMin() << "\n";
	std::cout << "Remove3: " << a.removeMin() << "\n";
	std::cout << "IsEmpty: " << a.isItEmpty() << "\n";
	system("PAUSE");
	std::cout << "Remove4: " << a.removeMin() << "\n"; //zwraca '-1' jako b��d
	system("PAUSE");

	std::cout << "____________________" << "\n\n";
	for (int i = 15; i > 0; i--)
		b.pushBack(i);

	std::cout << "Dodano: \n";
	b.print();
	std::cout << "\nSortowanie... \n";
	prioritySort(b);
	b.print();

	system("PAUSE");
	return 0;
}