#pragma once
#include "dList.h"

template <typename Type>
class SeqNode : public dList<Type>
{
public:
	SeqNode();
	~SeqNode();

	mNode<Type>* atIndex(int);
	int indexOf(mNode<Type>*);
};

#include "SeqNode.tpp"