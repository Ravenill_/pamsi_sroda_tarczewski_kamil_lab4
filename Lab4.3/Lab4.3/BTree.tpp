#include <iostream>
#include "BTree.h"

template<typename Type>
BTree<Type>::BTree()
{
	root = nullptr;
}

template<typename Type>
BTree<Type>::~BTree()
{
	destroy();
}
//______________________________________________________________
template<typename Type>
void BTree<Type>::destroy(BtNode<Type>* leaf)//private
{
	if (leaf != nullptr)
	{
		destroy((*leaf).getLeft());
		destroy((*leaf).getRight());
		delete leaf;
	}
}

template<typename Type>
void BTree<Type>::destroy()
{
	destroy(root);
}
//______________________________________________________________
template<typename Type>
BtNode<Type>* BTree<Type>::search(Type data, BtNode<Type>* leaf)//private
{
	if (leaf != nullptr)
	{
		if (data == (*leaf).getData())
			return leaf;
		else if (data < (*leaf).getData())
			return search(data, (*leaf).getLeft());
		else
			return search(data, (*leaf).getRight());
	}
	else
	{
		std::cerr << "Nie znaleziono wezla o takiej zawartosci\n";
		return nullptr;
	}
}

template<typename Type>
BtNode<Type>* BTree<Type>::search(Type data)
{
	return search(data, root);
}
//_____________________________________________________________
template<typename Type>
void BTree<Type>::insert(Type data, BtNode<Type>* leaf)//private
{
	if (data < (*leaf).getData())
	{
		if ((*leaf).getLeft() != nullptr)
			insert(data, (*leaf).getLeft());
		else
		{
			BtNode<Type> *newElem = new BtNode<Type>;
			(*newElem).setData(data);
			(*leaf).setLeft(newElem);
			(*newElem).setFather(leaf);
		}
			
	}
	else
	{
		if ((*leaf).getRight() != nullptr)
			insert(data, (*leaf).getRight());
		else
		{
			BtNode<Type> *newElem = new BtNode<Type>;
			(*newElem).setData(data);
			(*leaf).setRight(newElem);
			(*newElem).setFather(leaf);
		}
			
	}
}

template<typename Type>
void BTree<Type>::insert(Type data)
{
	if (root == nullptr)
	{
		BtNode<Type> *newElem = new BtNode<Type>;
		(*newElem).setData(data);
		root = newElem;
	}
	else
		insert(data, root);
}
//_______________________________________________________
template<typename Type>
int BTree<Type>::height(BtNode<Type>* leaf)//private
{
	if (isExternal(leaf))
		return 0;
	else
	{
		int h = 0;
		h = maxMin(h, height((*leaf).getLeft()));
		h = maxMin(h, height((*leaf).getRight()));
		return 1 + h;
	}

}

template<typename Type>
int BTree<Type>::height()
{
	return height(root);
}
//_______________________________________________________
template<typename Type>
void BTree<Type>::printNode(Type data)
{
	std::cout << "W pierwszym znalezionym wezle o zawartosci " << data << " sa: \n";
	if ((*search(data)).getLeft() != NULL && (*search(data)).getRight() != NULL)
		std::cout << (*(*search(data)).getLeft()).getData() << " oraz " << (*(*search(data)).getRight()).getData() << "\n";
	else if ((*search(data)).getLeft() != NULL)
		std::cout << (*(*search(data)).getRight()).getData() << "\n";
	else if ((*search(data)).getRight() != NULL)
		std::cout << (*(*search(data)).getLeft()).getData() << "\n";
	else
		std::cout << "Wezel nie ma dzieci\n";
}

template<typename Type>
bool BTree<Type>::isExternal(BtNode<Type>* leaf)
{
	if ((*leaf).getLeft() == nullptr && (*leaf).getRight() == nullptr)
		return true;
	else
		return false;
}

template<typename Type>
void BTree<Type>::preOrder(BtNode<Type>* leaf)
{
	std::cout << (*leaf).getData() << " ";
	if ((*leaf).getLeft() != nullptr)
		preOrder((*leaf).getLeft());
	if ((*leaf).getRight() != nullptr)
		preOrder((*leaf).getRight());
}

template<typename Type>
void BTree<Type>::postOrder(BtNode<Type>* leaf)
{
	if ((*leaf).getLeft() != nullptr)
		postOrder((*leaf).getLeft());
	if ((*leaf).getRight() != nullptr)
		postOrder((*leaf).getRight());
	std::cout << (*leaf).getData() << " ";
}

template<typename Type>
void BTree<Type>::inOrder(BtNode<Type>* leaf)
{
	if ((*leaf).getLeft() != nullptr)
		inOrder((*leaf).getLeft());
	std::cout << (*leaf).getData() << " ";
	if ((*leaf).getRight() != nullptr)
		inOrder((*leaf).getRight());
}

//_________________________________________________________
int maxMin(int a, int b)
{
	if (a > b)
		return a;
	else
		return b;
}