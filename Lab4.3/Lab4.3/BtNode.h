#pragma once

template <typename Type>
class BtNode
{
private:
	Type data;
	BtNode<Type> *father;
	BtNode<Type> *left;
	BtNode<Type> *right;

public:
	BtNode() : left(nullptr), right(nullptr), father(nullptr) {};

	Type getData() { return data; }
	void setData(Type value) { data = value; }
	BtNode<Type>* getLeft() { return left; }
	void setLeft(BtNode<Type>* node) { left = node; }
	BtNode<Type>* getRight() { return right; }
	void setRight(BtNode<Type>* node) { right = node; }
	BtNode<Type>* getFather() { return father; }
	void setFather(BtNode<Type>* node) { father = node; }
};