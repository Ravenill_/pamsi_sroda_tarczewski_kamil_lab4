#pragma once
#include "BtNode.h"

template <typename Type>
class BTree
{
private:
	BtNode<Type> *root;

	void destroy(BtNode<Type>*);
	BtNode<Type>* search(Type, BtNode<Type>*);
	void insert(Type, BtNode<Type>*);
	bool isExternal(BtNode<Type>*);
	int height(BtNode<Type>*);

	void preOrder(BtNode<Type>*);
	void postOrder(BtNode<Type>*);
	void inOrder(BtNode<Type>*);

public:
	BTree();
	~BTree();

	void insert(Type);
	BtNode<Type>* search(Type);
	void destroy();
	void printNode(Type);
	int height();

	void preOrder() { preOrder(root); }
	void postOrder() { postOrder(root); }
	void inOrder() { inOrder(root); }
};

int maxMin(int, int);

#include "BTree.tpp"