#include "BTree.h"
#include <cstdlib>

int main()
{
	BTree<int> a;
	a.insert(2);
	a.insert(4);
	a.insert(1);

	a.printNode(4);
	std::cout << a.height() << "\n";
	system("PAUSE");

	std::cout << "\nPreOrder: \n";
	a.preOrder();
	system("PAUSE");

	std::cout << "\nPostOrder: \n";
	a.postOrder();
	system("PAUSE");

	std::cout << "\nInOrder: \n";
	a.inOrder();
	system("PAUSE");
}