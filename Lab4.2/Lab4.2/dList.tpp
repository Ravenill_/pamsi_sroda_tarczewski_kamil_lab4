#include <string>
#include "dList.h"

template <typename Type>
dList<Type>::dList()
{
	first = NULL;
	last = NULL;
	size = 0;
}

template <typename Type>
dList<Type>::~dList()
{

}

template <typename Type>
int dList<Type>::rozmiar()
{
	/*int size = 0;

	//licz, p�ki nie przekieruje Ci� na NULLA z ostatniego elementu
	for (mNode<Type> *temp = first; temp != NULL; temp = (*temp).getNext())
		size += 1;

	return size;*/
	return size;
}

template <typename Type>
bool dList<Type>::isEmpty()
{
	if (first == NULL)
		return true;
	else
		return false;
}

template <typename Type>
Type dList<Type>::pierwszy()
{
	try
	{
		mNode<Type> *temp = first;
		Type result;

		if (temp != NULL)
			result = (*temp).getData();
		else
		{
			std::string w = "EmptydListException";
			throw w;
		}
		return result;
	}
	catch (std::string w)
	{
		std::cerr << w << "\n";
		return -1;
	}
}

template <typename Type>
Type dList<Type>::ostatni()
{
	try
	{
		mNode<Type> *temp = last;
		Type result;

		if (temp != NULL)
			result = (*temp).getData();
		else
		{
			std::string w = "EmptydListException";
			throw w;
		}
		return result;
	}
	catch (std::string w)
	{
		std::cerr << w << "\n";
		return -1;
	}
}

template <typename Type>
void dList<Type>::wstawPierwszy(Type obj)
{
	//nowy element
	mNode<Type> *newElem;
	newElem = new mNode<Type>;

	//wrzu� na prz�d
	(*newElem).setNext(first);
	(*newElem).setPrev(NULL);
	(*newElem).setData(obj);

	if (first == NULL)
		last = newElem;
	else
		(*(*newElem).getNext()).setPrev(newElem);

	first = newElem;
	size++;
}

template <typename Type>
Type dList<Type>::usunPierwszy()
{
	try
	{
		mNode<Type> *temp = first;
		Type result;

		if (temp != NULL)//usu�, je�li jest co� na li�cie
		{
			result = (*first).getData();
			first = (*temp).getNext();
			if (first != NULL)
				(*first).setPrev(NULL);
			else
				last = NULL;
			delete temp;
		}
		else
		{
			std::string w = "EmptydListException";
			throw w;
		}
		size--;
		return result;
	}
	catch (std::string w)
	{
		std::cerr << w << "\n";
		return -1;
	}
}

template <typename Type>
void dList<Type>::wstawOstatni(Type obj)
{
	//deklaracja i inicjalizacja
	mNode<Type> *newElem, *temp;

	temp = first;
	newElem = new mNode<Type>;
	(*newElem).setNext(NULL);
	(*newElem).setPrev(NULL);
	(*newElem).setData(obj);

	if (temp == NULL)
		first = newElem;
	else
	{
		temp = last;
		(*temp).setNext(newElem);
		(*newElem).setPrev(temp);
	}

	size++;
	last = newElem;
}

template <typename Type>
Type dList<Type>::usunOstatni()
{
	try
	{
		mNode<Type>* temp = last;
		Type result;

		if (temp != NULL)//usu�, je�li jest co� na li�cie
		{
			result = (*last).getData();
			last = (*temp).getPrev();
			if (last != NULL)
				(*last).setNext(NULL);
			else
				first = NULL;
			delete temp;
		}
		else
		{
			std::string w = "EmptydListException";
			throw w;
		}
		size--;
		return result;
	}
	catch (std::string w)
	{
		std::cerr << w << "\n";
		return -1;
	}
}

template<typename Type>
void dList<Type>::wstaw(Type obj, mNode<Type>* here)
{
	//deklaracja
	mNode<Type> *newElem = new mNode<Type>;
	(*newElem).setData(obj);
	(*newElem).setNext(NULL);
	(*newElem).setPrev(NULL);

	(*newElem).setPrev(here);
	if (here == NULL)//je�li pusta
	{
		first = newElem;
		last = newElem;
	}
	else if (here != NULL)//je�li niepusta
	{
		if ((*here).getNext() == NULL)
			last = newElem;
		if ((*here).getNext() != NULL)//gdy nie ostatni
		{
			(*newElem).setNext((*here).getNext());
			(*(*here).getNext()).setPrev(newElem);
		}
		(*here).setNext(newElem);
	}
	size++;
}

template<typename Type>
Type dList<Type>::usun(mNode<Type>* here)
{
	//tymczasowa
	mNode<Type> *temp = (*here).getPrev();
	Type result = (*here).getData();

	if ((*here).getNext == NULL)//je�li ostatni
		(*temp).setNext(NULL);
	else//je�li nieostatni
	{
		(*(*here).getNext()).setPrev(temp);
		(*temp).setNext((*here).getNext());
	}
	delete here;
	here = NULL;
	size--;

	return result;
}

template<typename Type>
mNode<Type>* dList<Type>::wez(int i)
{
	mNode<Type>* temp = first;

	if ((i < size) && (i >= 0))
	{
		for (int a = 0; a < i; a++)
			temp = (*temp).getNext();
		return temp;
	}
	else
		return NULL;
}

template<typename Type>
Type dList<Type>::operator[](int a)
{
	if (wez(a) == NULL)
		return Type();
	else
	{
		return (*wez(a)).getData();
	}
}
