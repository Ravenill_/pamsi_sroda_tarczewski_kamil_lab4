#pragma once
#include "dList.h"

template <typename Type>
class tNode
{
private:
	Type data;
	tNode<Type>* father;
	dList<tNode<Type>*> kids;

public:
	tNode() : father(NULL) {};
	~tNode() {};

	tNode<Type>* getFather() { return father; }
	void setFather(tNode<Type> *a) { father = a; }
	tNode<Type>* getKids(int i) { return kids[i]; }
	dList<tNode<Type>*> getKids() { return kids; }
	void setKids(tNode<Type> *a) { kids.wstawOstatni(a); }
	Type getData() { return data; }
	void setData(Type a) { data = a; }
};