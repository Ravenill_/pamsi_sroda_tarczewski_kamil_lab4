#include <iostream>
#include "Tree.h"

template<typename Type>
Tree<Type>::Tree()
{
	root = nullptr;
}

template<typename Type>
Tree<Type>::~Tree()
{
	destroy();
}
//______________________________________________________________
template<typename Type>
void Tree<Type>::destroy(tNode<Type>* leaf)//private
{
	if (leaf != nullptr)
	{
		for (int i = 0; i < (*leaf).getKids().rozmiar(); i++)
			postOrder((*leaf).getKids(i));
		delete leaf;
	}
}

template<typename Type>
void Tree<Type>::destroy()
{
	destroy(root);
}
//_____________________________________________________________
template<typename Type>
void Tree<Type>::insert(Type data, Type here)
{
	tNode<Type> *newElem = new tNode<Type>;
	(*newElem).setData(data);

	if (root == nullptr)
	{
		root = newElem;
	}
	else
	{
		tNode<Type> *temp = preOrder(here);
		(*temp).setKids(newElem);
		(*newElem).setFather(temp);
	}
}
//_______________________________________________________
template<typename Type>
int Tree<Type>::height(tNode<Type>* leaf)//private
{
	if (isExternal(leaf))
		return 0;
	else
	{
		int h = 0;
		for(int i = 0; i < (*leaf).getKids().rozmiar(); i++)
			h = maxMin(h, height((*leaf).getKids(i)));
		return 1 + h;
	}

}

template<typename Type>
int Tree<Type>::height()
{
	return height(root);
}
//_______________________________________________________
template<typename Type>
void Tree<Type>::preOrder(tNode<Type>* leaf)
{
	std::cout << (*leaf).getData() << " ";
	for (int i = 0; i < (*leaf).getKids().rozmiar(); i++)
		preOrder((*leaf).getKids(i));
}

template<typename Type>
tNode<Type>* Tree<Type>::preOrder(tNode<Type>* leaf, Type limit)
{
	if ((*leaf).getData() == limit) return leaf;

	for (int i = 0; i < (*leaf).getKids().rozmiar(); i++)
		return preOrder((*leaf).getKids(i), limit);

	return root;
}

template<typename Type>
void Tree<Type>::postOrder(tNode<Type>* leaf)
{
	for (int i = 0; i < (*leaf).getKids().rozmiar(); i++)
		postOrder((*leaf).getKids(i));
	std::cout << (*leaf).getData() << " ";
}

//________________________________________________________
template<typename Type>
void Tree<Type>::printNode(Type data)
{
	std::cout << "W pierwszym znalezionym wezle o zawartosci " << data << " sa: \n";
	for(int i = 0; i < (*preOrder(root, data)).getKids().rozmiar(); i++)
		std::cout << (*preOrder(root, data)).getKids(i).getData() << " ";
}

template<typename Type>
bool Tree<Type>::isExternal(tNode<Type>* leaf)
{
	if ((*leaf).getKids().rozmiar() == 0)
		return true;
	else
		return false;
}

int maxMin(int a, int b)
{
	if (a > b)
		return a;
	else
		return b;
}