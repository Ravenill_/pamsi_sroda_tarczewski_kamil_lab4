#include "Tree.h"
#include <cstdlib>

int main()
{
	Tree<int> a;
	a.insert(9, 1);
	a.insert(3, 9);
	a.insert(7, 9);
	a.insert(8, 9);
	a.insert(1, 3);
	a.insert(2, 3);
	a.insert(4, 7);
	a.insert(5, 7);
	a.insert(6, 7);

	std::cout << "PostOrder:\n";
	a.postOrder();
	system("PAUSE");

	std::cout << "\PreOrder:\n";
	a.preOrder();
	system("PAUSE");

	std::cout << "\nHeight: " << a.height();
	system("PAUSE");
}