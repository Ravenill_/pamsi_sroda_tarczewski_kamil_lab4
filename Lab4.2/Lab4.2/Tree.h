#pragma once
#include "tNode.h"

template <typename Type>
class Tree
{
private:
	tNode<Type>* root;

	void destroy(tNode<Type>*);
	bool isExternal(tNode<Type>*);
	int height(tNode<Type>*);

	void preOrder(tNode<Type>*);
	tNode<Type>* preOrder(tNode<Type>*, Type);
	void postOrder(tNode<Type>*);

public:
	Tree();
	~Tree();

	void insert(Type data, Type here);
	void destroy();
	void printNode(Type data);
	int height();

	void preOrder() { preOrder(root); }
	tNode<Type>* preOrder(Type limit) { return preOrder(root, limit); }
	void postOrder() { postOrder(root); }
};

#include "Tree.tpp"